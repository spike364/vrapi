import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus } from '@nestjs/common';
import { validate } from 'class-validator';
import { Reservation } from './entities/reservation.entity';
import { CreateReservationDTO } from './dto/create-reservation.dto';
import { ReservationRO } from './reservation.interface';

@Injectable()
export class ReservationsService {
  constructor(
    @InjectRepository(Reservation)
    private reservationsRepository: Repository<Reservation>,
  ) {}

  async getReservations(): Promise<any> {
    return await this.reservationsRepository.find();
  }

  async addReservation(): Promise<any> {
    return await this.reservationsRepository.find();
  }

  async getReservedTimesByDateAndSpot(
    spot: string,
    date: string,
  ): Promise<any> {
    const reservedDates = await (await this.reservationsRepository.find())
      .filter(item => {
        return item.spot === spot;
      })
      .filter(
        item =>
          new Date(item.date).getDate() === new Date(date).getDate() &&
          new Date(item.date).getMonth() === new Date(date).getMonth() &&
          new Date(item.date).getFullYear() === new Date(date).getFullYear(),
      )
      .map(item => {
        const gameStart = new Date(item.date);
        const gameEnd = new Date(
          new Date(item.date).getTime() +
            parseInt(item.playtime.replace(/\D/g, '')) * 60000,
        );
        const countedGameEnd = new Date(
          gameEnd.setMinutes(gameEnd.getMinutes() + 5),
        );
        return this.getReservedTimeByGame(gameStart, countedGameEnd);
      });

    return reservedDates;
  }

  async createReservation(dto: CreateReservationDTO): Promise<ReservationRO> {
    const {
      action,
      name,
      phone,
      spot,
      price,
      playtime,
      date,
      dateTimestamp,
      bonusTime,
      email,
      players,
      status,
      source,
    } = dto;

    const newReservation = new Reservation();
    newReservation.action = action;
    newReservation.name = name;
    newReservation.phone = phone;
    newReservation.email = email;
    newReservation.date = date;
    newReservation.dateTimestamp = dateTimestamp;
    newReservation.price = price;
    newReservation.playtime = playtime;
    newReservation.bonusTime = bonusTime;
    newReservation.spot = spot;
    newReservation.players = players;
    newReservation.status = status;
    newReservation.source = source;

    const errors = await validate(newReservation);
    if (errors.length > 0) {
      const _errors = { email: 'Email is not valid.' };
      throw new HttpException(
        { message: 'Input data validation failed', _errors },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedReservation = await this.reservationsRepository.save(
        newReservation,
      );
      return this.buildReservationRO(savedReservation);
    }
  }

  async getAvailableDates(): Promise<any> {
    const qb = await this.reservationsRepository
      .createQueryBuilder()
      .select('reservation')
      .from(Reservation, 'reservation')
      .where('reservation.dateTimestamp > UNIX_TIMESTAMP()');

    const availableDates = await qb.getMany();

    return availableDates;
  }

  private buildReservationRO(reservation: Reservation) {
    const reservationRO = {
      id: reservation.id,
      action: reservation.action,
      email: reservation.email,
      name: reservation.name,
      phone: reservation.phone,
      date: reservation.date,
      dateTimestamp: reservation.dateTimestamp,
      players: reservation.players,
      playtime: reservation.playtime,
      spot: reservation.spot,
      price: reservation.price,
      status: reservation.status,
      source: reservation.source,
      bonusTime: reservation.bonusTime,
    };

    return { reservation: reservationRO };
  }

  private getDate(dateString: string) {
    const day: any = dateString.split(' ')[0].split('.')[0];
    const month: any = Number(dateString.split(' ')[0].split('.')[1]) - 1;
    const year: any = dateString.split(' ')[0].split('.')[2];
    const hour: any = dateString.split(' ')[1].split(':')[0];
    const min: any = dateString.split(' ')[1].split(':')[1];

    return new Date(year, month, day, hour, min);
  }

  private getReservedTimeByGame(start: Date, end: Date) {
    const dates = [];
    dates.push(start);
    while (
      start.getHours() < end.getHours() ||
      start.getMinutes() < end.getMinutes()
    ) {
      dates.push(new Date(start.setMinutes(start.getMinutes() + 5)));
    }
    return dates;
  }
}
