export interface ReservationData {
  email: string;
  name: string;
  action: string;
  date: string;
  phone: string;
  players: string;
  playtime: string;
  spot: string;
  price: string;
  bonusTime: string;
  status: string;
  source: string;
}

export interface ReservationRO {
  reservation: ReservationData;
}
