import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Reservation {
  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: Date;

  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  action: string;

  @Column()
  date: string;

  @Column()
  dateTimestamp: string;

  @Column()
  email: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  players: string;

  @Column()
  playtime: string;

  @Column()
  spot: string;

  @Column()
  price: string;

  @Column()
  bonusTime: string;

  @Column()
  status: string;

  @Column()
  source: string;
}
