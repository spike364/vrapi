import { Controller, Get, Post, Body, Header, Query } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { CreateReservationDTO } from './dto/create-reservation.dto';

@Controller('reservations')
export class ReservationsController {
  constructor(private reservationsService: ReservationsService) {}

  @Get()
  async getReservations() {
    const reservations = await this.reservationsService.getReservations();
    return reservations;
  }

  @Get('dates')
  @Header('Access-Control-Allow-Origin', '*')
  async getReservedTimesByDateAndSpot(@Query() data: any) {
    const reservedDates = await this.reservationsService.getReservedTimesByDateAndSpot(
      data.spot,
      data.date,
    );
    return reservedDates;
  }

  @Post()
  async create(@Body() reservation: CreateReservationDTO) {
    return this.reservationsService.createReservation(reservation);
  }
}
