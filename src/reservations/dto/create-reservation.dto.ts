import { IsNotEmpty } from 'class-validator';

export class CreateReservationDTO {
  @IsNotEmpty()
  readonly created: Date;

  @IsNotEmpty()
  readonly action: string;

  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  readonly email: string;

  @IsNotEmpty()
  readonly phone: string;

  @IsNotEmpty()
  readonly spot: string;

  @IsNotEmpty()
  readonly date: string;

  @IsNotEmpty()
  readonly dateTimestamp: string;

  @IsNotEmpty()
  readonly playtime: string;

  @IsNotEmpty()
  readonly price: string;

  @IsNotEmpty()
  readonly players: string;

  @IsNotEmpty()
  readonly bonusTime: string;

  @IsNotEmpty()
  readonly status: string;

  @IsNotEmpty()
  readonly source: string;
}
